<?php

$email = $_SESSION['email'];
$con = new mysqli("localhost", "root", "", "vapt");
$consulta = "select nome,imgPerfil,apelido,sexo,telefone,cep,nCasa,complemento,bairro,estado,endereco from usuarios where email='$email'";
$r = mysqli_query($con, $consulta) or die("erro");

while ($linha = mysqli_fetch_assoc($r)) {
    $nome = $linha['nome'];
    $imgPerfil = $linha['imgPerfil'];
    $apelido = $linha['apelido'];
    $sexo = $linha['sexo'];
    $telefone = $linha['telefone'];
    $cep = $linha['cep'];
    $nCasa = $linha['nCasa'];
    $complemento = $linha['complemento'];
    $bairro = $linha['bairro'];
    $estado = $linha['estado'];
    $endereco = $linha['endereco'];

    echo "

    <!-- perfil -->

    <div class='container'>
    <div class='row'>
        <div class='col-md-5 dados'>
            <h3>Informações do Perfil</h3>

            <form action='atualizaPerfil.php?email=$email' method='post'>
            <h4>Dados do Usuário</h4>
            <div class='form-group'>
                <label for='name'>Nome Completo</label>
                <input type='text' class='form-control' id='name' name='nome' value='$nome'>
            </div>
            <div class='form-group'>
                <label for='apelido'>Apelido</label>
                <input type='text' class='form-control' id='apelido' name='apelido' value='$apelido'>
            </div>
            <h4>Dados de Contato</h4>
            <div class='form-group'>
                <label for='email'>E-mail</label>
                <input type='text' class='form-control' id='email' name='email' value='$email'>
            </div>
            <div class='form-group'>
                <label for='telefone'>Número de Telefone/Celular</label>
                <input type='number' class='form-control' id='telefone' name='telefone' value='$telefone'>
            </div>
            <h4>Dados de Localização</h4>
            <div class='form-group'>
                <label for='cep'>CEP</label>
                <input type='number' class='form-control' id='cep' name='cep' value='$cep'>
            </div>
            <div class='form-group'>
                <label for='endereco'>Endereço</label>
                <input type='text' class='form-control' id='endereco' name='endereco' value='$endereco'>
            </div>
            <div class='form-group col-xs-4 f-inp'>
                <label for='numero'>Número</label>
                <input type='number' class='form-control' id='numero' name='nCasa' value='$nCasa'>
            </div>
            <div class='form-group col-xs-8 f-inp-comp'>
                <label for='complemento'>Complemento</label>
                <input type='text' class='form-control' id='complemento' name='complemento' value='$complemento'>
            </div>
            <div class='form-group'>
                <label for='bairro'>Bairro </label>
                <input type='text' class='form-control' id='bairro' name='bairro' value='$bairro'>
            </div>
            <label for='estado'>Estado</label>
            <select name='estado' class='form-control'>
                <option value='$estado'>$estado</option>
                <option value=''>Selecione seu estado</option>
                <option value='Acre'>Acre</option>
                <option value='Alagoas'>Alagoas</option>
                <option value='Amapá'>Amapá</option>
                <option value='Amazonas'>Amazonas</option>
                <option value='Bahia'>Bahia</option>
                <option value='Ceará'>Ceará</option>
                <option value='Distrito Federal'>Distrito Federal</option>
                <option value='Espírito Santo'>Espírito Santo</option>
                <option value='Goiás'>Goiás</option>
                <option value='Maranhão'>Maranhão</option>
                <option value='Mato Grosso'>Mato Grosso</option>
                <option value='Mato Grosso do Sul'>Mato Grosso do Sul</option>
                <option value='Minas Gerais'>Minas Gerais</option>
                <option value='Pará'>Pará</option>
                <option value='Paraíba'>Paraíba</option>
                <option value='Paraná'>Paraná</option>
                <option value='Pernambuco'>Pernambuco</option>
                <option value='Piauí'>Piauí</option>
                <option value='Rio de Janeiro'>Rio de Janeiro</option>
                <option value='Rio Grande do Norte'>Rio Grande do Norte</option>
                <option value='Rio Grande do Sul'>Rio Grande do Sul</option>
                <option value='Rondônia'>Rondônia</option>
                <option value='Roraima'>Roraima</option>
                <option value='Santa Catarina'>Santa Catarina</option>
                <option value='São Paulo'>São Paulo</option>
                <option value='Sergipe'>Sergipe</option>
                <option value='Tocantins'>Tocantins</option>
            </select>
            <button type='submit' class='btn btn-primary botao'>
                Salvar Informações
            </button>

            </form>
        </div>
        <!-- col -->
        <form action='imgPerfil.php?email=$email' method='post' enctype='multipart/form-data'>
        <div class='col-md-5 imagem'>
            <h4>Altere sua imagem de perfil aqui!</h4>
            <img src='$imgPerfil' alt=''>
            <div class='form-group form-img'>
                <label for='selectImagem'>Selecione sua Imagem</label>
                <input type='file' id='selectImagem' name='imgPerfil'>
                <button type='submit' class='btn btn-primary botao'>
                Alterar
            </button>
                </div>
            </form>
        </div>
        <div class='col-md-5 senha'>
            <h4>Senha Cadastrada</h4>
            <h5>Caso seja necessário, altere sua senha abaixo!</h5>
                <a href=''>
                    <h5>Esqueceu sua senha? Clique aqui!</h5>
                </a>
            <div class='form-group'>
            <form action='updateSenha.php?email=$email' method='post'>
                <label for='nova'>Nova Senha</label>
                <input id='txtSenha' name='senha' class='form-control' type='password' required placeholder='Digite a nova senha' title='Senha' />
            </div>
            <div class='form-group'>
                <label for='confir'>Confirmar Senha</label>
                <input id='repetir_Senha' class='form-control' name='' type='password' required  placeholder='Repetir Senha' title='Repetir Senha' oninput='validaSenha(this)' />
            </div>
            <button type='submit' class='btn btn-primary botao'>
                Alterar Senha
            </button>
            </form>
        </div>
        <!-- col -->
    </div>
    <!-- row -->
</div>
<!-- container -->

    ";

}
