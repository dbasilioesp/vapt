<div class='container'>
    <div class='row'>
        <div class='col-md-4 col-ceta'>
            <div class='ponta-ceta'></div>
            <div class='ceta'>Preço</div>
            <div class='col-md-12 inf'>
                <table style="width:100%">
                    <tr>
                        <th class='informacao'>Vendedor</th>
                        <th colspan="2" class='val-info'>Maulei23</th>
                    </tr>
                    <tr>
                        <th class='informacao'>Categoria</th>
                        <th colspan="2" class='val-info'>Informática</th>
                    </tr>
                    <tr>
                        <th class='informacao'>Local</th>
                        <th colspan="2" class='val-info'>Porto Alegre</th>
                    </tr>
                    <tr>
                        <th class='informacao'>Estado do Produto</th>
                        <th colspan="2" class='val-info'>Produto Novo</th>
                    </tr>
                </table>
                <div class='descricao'>
                    <h4>Descrição</h4>
                    <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent metus tellus, condimentum sed venenatis
                        ac, pretium id nunc. Phasellus vehicula congue tincidunt. Nulla sed lacus vulputate, laoreet augue
                        at, pretium est. Etiam maximus nisl gravida erat aliquet elementum. Nunc consequat lectus et orci
                        dapibus gravida. Donec laoreet efficitur facilisis. Sed a metus sed dolor ullamcorper sollicitudin.
                        Aenean semper, eros et posuere hendrerit, risus ipsum pharetra enim, non pulvinar neque est et massa.
                        Nulla suscipit ex ac elit ullamcorper tempor.</h5>
                </div>
            </div>
        </div>
        <div class='col-md-8 titulo-view'>
            <h2>Titulo do Anuncio</h2>
            <h5>Data da Postagem</h5>
            <div class='slider-for slider-css'>
                <!-- Item -->
                <div class="produtos ">
                    <img src="imagens\produtos\prod-tv.png">
                </div>
                <!-- Item -->
                <div class="produtos ">
                    <img src="imagens\produtos\pc.png">
                </div>
                <!-- Item -->
                <div class="produtos ">
                    <img src="imagens\produtos\pc.png">
                </div>
                <!-- Item -->
                <div class="produtos ">
                    <img src="imagens\produtos\roupeiro.png">
                </div>
            </div>
        </div>
        <div class='col-md-8'>
            <div class='slider-nav slider-nav-css'>
                <!-- Item -->
                <div class="view-produtos ">
                    <img src="imagens\produtos\prod-tv.png">
                </div>
                <div class="view-produtos ">
                    <img src="imagens\produtos\pc.png">
                </div>
                <div class="view-produtos ">
                    <img src="imagens\produtos\pc.png">
                </div>
                <div class="view-produtos ">
                    <img src="imagens\produtos\roupeiro.png">
                </div>
                <div class="view-produtos ">
                    <img src="imagens\produtos\roupeiro.png">
                </div>
            </div>
        </div>
        <div class='col-md-8 comentario'>
            <h1>Comentários</h1>
            <h3>Maulei23</h3>
            <h4>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent metus tellus, condimentum sed venenatis ac, pretium id
                nunc. Phasellus vehicula congue tincidunt. Nulla sed lacus vulputate, laoreet augue at, pretium est.
            </h4>
            <h3>NataGi</h3>
            <h4>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent metus tellus, condimentum sed venenatis ac, pretium id
                nunc. Phasellus vehicula congue tincidunt. Nulla sed lacus vulputate, laoreet augue at, pretium est.
            </h4>
        </div>
        <div class='col-md-4 comentar'>
            <h3>Deixe seu Comentário</h3>
            <div class='form-group'>
                <label for='titulo'>Nome</label>
                <input required='required' type='text' class='form-control' id='titulo' name='nomeComentarista' required='required' placeholder='Digite seu nome'>
            </div>
            <div class='form-group'>
                <textarea class='form-control' rows='3' required='required' name='comentario'></textarea>
            </div>
            <button type='submit' class='btn botao-modal'>
                Enviar
            </button>
        </div>
    </div>
</div>