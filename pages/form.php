<h3 class='title'>Faça seu login ou crie uma conta gratuita!</h3>
    <p class='title'>Veja, edite ou exclua seus anúncios em um só lugar de forma rápida e fácil.</p>
<div class='divs-login-cad'>

    <div class='container'>
        <div class='row'>
            <div class='col-md-4'>
            <form action='actions/login.php' method='post'>
                <h4>Faça seu Login</h4>
                <div class='form-group'>
                    <input type='text' class='form-control' id='email' name='email' placeholder='Digite seu E-mail'>
                </div>
                <div class='form-group'>
                    <input type='password' class='form-control' id='senha' name='senha' placeholder='Digite sua Senha'>
                </div>
                <a href=''>
                    <h5>Esqueceu sua senha? Clique aqui!</h5>
                </a>
                <button type='submit' class='btn btn-primary botao'>
                    Login
                </button>
            </form>
            </div>
            <!-- /col -->
            <div class='col-md-4  borda'>
            <form action='actions/cadastroCli.php' method='post'>
                <h4>Faça seu Cadastro</h4>
                <div class='form-group'>
                    <input type='text' class='form-control' id='nome' name='nome' placeholder='Digite seu Nome'>
                </div>
                <div class='form-group'>
                <input id="txtSenha" name="senha" class='form-control' type="password" required placeholder="Digite sua Senha" title="Senha" />
                </div>
                <div class='form-group'>
                <input id="repetir_Senha" class='form-control' name="" type="password" required  placeholder="Repetir Senha" title="Repetir Senha" oninput="validaSenha(this)" />
                </div>
                <div class='form-group'>
                <input required class='form-control' id="txtEmail" name="email" type="email" placeholder="Digite seu Email" title="Email" />
                </div>
                <div class='form-group'>
                <input class='form-control' id="repetir_Email" name="" type="email" required  placeholder="Repetir Email" title="Repetir Email" oninput="validaEmail(this)" />
                </div>
                <p class='termo'>
                    Ao se cadastrar, eu concordo com os
                    <a href='' data-toggle='modal' data-target='#termos'>Termos de Uso</a> e a
                    <a href='' data-toggle='modal' data-target='#politica'>Política de Privacidade</a> da Vapt Trader, e também, em receber comunicações da Vapt Trader, por exemplo,
                    mensagens via e-mail de compradores interessados, promoções da Vapt Trader, dicas e recomendações sobre
                    produtos e serviços.
                </p>
                <button type='submit' class='btn btn-primary botao'>
                    Cadastrar
                </button>
                </form>
            </div>
            <!-- /col -->
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
