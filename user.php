<?php
session_start();
if ($_SESSION['validacao'] != 'user') {

    include 'layout/header.php';

    include 'layout/nav-user.php';

    include 'pages/prod.php';

    include 'layout/footer.php';

} else {
    echo "<script type='text/javascript'>
	alert('Acessor Negado!!');
	window.location='index.php';
</script>";
}
