<div class="divs-home">
  <h2>Produtos em Destaque</h2>
  <div class="imagens-home">
    <div class='container'>
      <div class='row'>
        <div class='col-md-12'>
          <div class='slideshow slideshow-css'>
            <!-- Item -->
            <div class="produtos ">
              <div class='img-'>
                <img src="assets/imagens\produtos\prod-tv.png">
              </div>
              <h3 class='f3'>R$ 400,00</h3>
              <h5>tv 22' lg</h5>
            </div>
            <!-- Item -->
            <div class="produtos ">
              <div class='img-'>
                <img src="assets/imagens\produtos\pc.png">
              </div>
              <h3 class='f3' >R$ 999,00</h3>
              <h5>all in one i3</h5>
            </div>
            <!-- Item -->
            <div class="produtos ">
              <img src="assets/imagens\produtos\pc.png">
              <h3 class='f3'>R$ 999,00</h3>
              <h5>all in one i3</h5>
            </div>
            <!-- Item -->
            <div class="produtos ">
              <img src="assets/imagens\produtos\roupeiro.png">
              <h3 class='f3'>R$ 789,99</h3>
              <h5>roupeiro kap</h5>
            </div>
            <!-- Item -->
            <div class="produtos ">
              <img src="assets/imagens\produtos\roupeiro.png">
              <h3 class='f3'>R$ 789,99</h3>
              <h5>roupeiro kap</h5>
            </div>
            <!-- Item -->
            <div class="produtos ">
              <img src="assets/imagens\produtos\roupeiro.png">
              <h3 class='f3'>R$ 789,99</h3>
              <h5>roupeiro kap</h5>
            </div>
            <!-- Item -->
            <div class="produtos ">
              <img src="assets/imagens\produtos\roupeiro.png">
              <h3 class='f3'>R$ 789,99</h3>
              <h5>roupeiro kap</h5>
            </div>
            <!-- Item -->
            <div class="produtos ">
              <img src="assets/imagens\produtos\roupeiro.png">
              <h3 class='f3'>R$ 789,99</h3>
              <h5>roupeiro kap</h5>
            </div>
          </div>
        </div>
        <!-- /col -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
</div>
