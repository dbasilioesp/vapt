<!DOCTYPE html>
<html lang='pt-br'>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

    <title>VAPT TRADER</title>
    <link rel='icon' href='assets/imagens/icone.png'>

    <!-- Link com as bibliotecas usadas -->

    <!-- Bootstrap -->
    <link href='assets/bootstrap/css/bootstrap.min.css' rel='stylesheet'>

    <!-- Link com o arquivo de css -->
    <link href='assets/styles\utilities.css' rel='stylesheet'>
    <link href='assets/styles\main.css' rel='stylesheet'>
    <link href='assets/styles\dispositivos.css' rel='stylesheet'>

    <!-- Slick -->
    <link rel="stylesheet" href="assets/js/slick/slick.css">
    <link rel="stylesheet" href="assets/js/slick/slick-theme.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
        crossorigin="anonymous">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src='https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js'></script>
      <script src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js'></script>
    <![endif]-->

       <!-- Valida Repetição Email  -->

    <script>

        function validaEmail(input) {
            if (input.value != document.getElementById('txtEmail').value) {
                input.setCustomValidity('Repita o Email corretamente');
            } else {
                input.setCustomValidity('');
            }
        } 
    </script>

      <!-- Valida Repetição Senha  -->

    <script>

        function validaSenha(input) {
            if (input.value != document.getElementById('txtSenha').value) {
                input.setCustomValidity('Repita a Senha corretamente');
            } else {
                input.setCustomValidity('');
            }
        } 
    </script>
</head>
