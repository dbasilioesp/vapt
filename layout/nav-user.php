<?php
  $email = $_SESSION['email'];
  $con = new mysqli("localhost","root","","vapt");
  $consulta="select nome,imgPerfil,apelido,sexo,telefone,cep,nCasa,complemento,bairro,estado,endereco from usuarios where email='$email'";
  $r = mysqli_query($con,$consulta) or die("erro");
  while($linha=mysqli_fetch_assoc($r)){
    $nome = $linha['nome'];
    $imgPerfil = $linha['imgPerfil'];
    $apelido = $linha['apelido'];
    $sexo = $linha['sexo'];
    $telefone = $linha['telefone'];
    $cep = $linha['cep'];
    $nCasa = $linha['nCasa'];
    $complemento = $linha['complemento'];
    $bairro = $linha['bairro'];
    $estado = $linha['estado'];
    $endereco = $linha['endereco'];


    echo"
    
    <body>
    <nav class='navbar  navbar-inverse navbar-transparente'>
        <div class='container'>
            <!-- header -->
            <div class='navbar-header'>
                <!-- botao toggle -->
                <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#barra-navegacao'>
                    <span class='sr-only'>alternar navegação</span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                </button>
                <a href='user.html' class='navbar-brand'>
                    <span class='img-logo'>Vapt Trader</span>
                </a>
            </div>
            <!-- navbar -->
            <div class='collapse navbar-collapse' id='barra-navegacao'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-xs-8'>
                            <ul class='nav navbar-nav navbar-right menu'>
                                <li class='dropdown'>
                                    <button class='btn dropdown-toggle drop' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>
                                        <img src='$imgPerfil' alt='' class='fotoPerfil'>
                                        <span class='glyphicon glyphicon-triangle-bottom color-glyphicon btn-lg'></span>
                                    </button>
                                    <ul class='dropdown-menu' aria-labelledby='dropdownMenu1'>
                                        <a class='dropdown-item' href='user.php'>Inicio</a>
                                        <a class='dropdown-item' href='perfil.php'>Perfil</a>
                                        <a class='dropdown-item' href='' data-toggle='modal' data-target='#janela'>Anunciar</a>
                                        <a class='dropdown-item' href='meusProdutos.php'>Meus Anúncios</a>
                                        <a class='dropdown-item' href='sair.php'>Sair</a>
                                    </ul>
                                </li>
                        </div>
                        <!-- /col -->
                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
        </div>
        <!-- /container -->
    </nav>
    <!-- /nav -->

    <!-- Janela -->
    <form class='modal fade' id='janela' action='cadastroAnuncio.php?email=$email' method='post' enctype='multipart/form-data'>
        <div class='modal-dialog modal-lg'>
            <div class='modal-content'>
                <!-- cabecalho -->
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal'>
                        <span>&times;</span>
                    </button>
                    <h4 class='modal-title'>Cadastrar Anúncio</h4>
                </div>
                <!-- corpo -->
                <div class='modal-body'>

                <div class='form-group warning'>
                        <label for='titulo'>Para acelerar o processo de anuncio, mantenha seu perfil atualizado!</label>
                        
                    </div>
            
                    <div class='form-group'>
                        <label for='titulo'>Título *</label>
                        <input required='required' type='text' class='form-control' id='titulo' name='titulo' required='required' placeholder='Digite o título do anúncio'>
                    </div>
                    <div class='form-group'>
                        <label for='descricao'>Descrição *</label>
                        <textarea class='form-control' rows='3' required='required' name='descricao'></textarea>
                    </div>
                    <div class='form-group'>
                        <label for='categoria'>Categoria *</label>
                        <select name='categoria' required='required' class='form-control'>
                            <option value=''>Selecione uma categoria</option>
                            <option value='Imoveis'>Imoveis</option>
                        </select>
                    </div>
                    <div class='form-group'>
                    <label for='tipo'>Tipo de Saída *</label>
                    <select name='tipo' required='required' class='form-control'>
                        <option value=''>Selecione</option>
                        <option value='venda.png'>Venda</option>
                        <option value='troca.png'>Troca</option>
                        <option value='troca-venda.png'>Venda e Troca</option>
                    </select>
                </div>
                <div class='form-group'>
                <label for='estadoProduto'>Estado do Produto</label>
                <select name='estadoProduto' class='form-control'>
                    <option value=''>Selecione</option>
                    <option value='Produto Novo'>Produto Novo</option>
                    <option value='Produto Usado'>Produto Usado</option>
                </select>
            </div>
                    <div class='form-group'>
                        <label for='contato'>Número de Contato *</label>
                        <input required='required' type='number' class='form-control' id='contato' name='nContato' value='$telefone'>
                    </div>
                    <div class='form-group'>
                        <label for='email'>E-mail</label>
                        <input required='required' type='email' class='form-control' id='email' name='emailAnuncio' value='$email'>
                    </div>
                    <div class='form-group'>
                        <label for='valor'>Valor *</label>
                        <div class='input-group'>
                            <div class='input-group-addon'>R$</div>
                            <input required='required' type='number' class='form-control' id='valor' name='valor'>
                            <div class='input-group-addon'>.00</div>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label for='cep'>CEP</label>
                        <input type='number' class='form-control' id='cep' name='cep' value='$cep'>
                    </div>
                    <div class='form-group'>
                        <label for='endereco'>Endereço *</label>
                        <input required='required' type='text' class='form-control' id='endereco' name='endereco' value='$endereco'>
                    </div>
                    <div class='form-group'>
                        <label for='bairro'>Bairro </label>
                        <input type='text' class='form-control' id='bairro' name='bairro' value='$bairro'>
                    </div>
                    <div class='form-group'>
                    <label for='estado'>Estado</label>
                    <select name='estado' class='form-control'>
                        <option value='$estado'>--- $estado ---</option>
                        <option value=''></option>
                        <option value='Acre'>Acre</option>
                        <option value='Alagoas'>Alagoas</option>
                        <option value='Amapá'>Amapá</option>
                        <option value='Amazonas'>Amazonas</option>
                        <option value='Bahia'>Bahia</option>
                        <option value='Ceará'>Ceará</option>
                        <option value='Distrito Federal'>Distrito Federal</option>
                        <option value='Espírito Santo'>Espírito Santo</option>
                        <option value='Goiás'>Goiás</option>
                        <option value='Maranhão'>Maranhão</option>
                        <option value='Mato Grosso'>Mato Grosso</option>
                        <option value='Mato Grosso do Sul'>Mato Grosso do Sul</option>
                        <option value='Minas Gerais'>Minas Gerais</option>
                        <option value='Pará'>Pará</option>
                        <option value='Paraíba'>Paraíba</option>
                        <option value='Paraná'>Paraná</option>
                        <option value='Pernambuco'>Pernambuco</option>
                        <option value='Piauí'>Piauí</option>
                        <option value='Rio de Janeiro'>Rio de Janeiro</option>
                        <option value='Rio Grande do Norte'>Rio Grande do Norte</option>
                        <option value='Rio Grande do Sul'>Rio Grande do Sul</option>
                        <option value='Rondônia'>Rondônia</option>
                        <option value='Roraima'>Roraima</option>
                        <option value='Santa Catarina'>Santa Catarina</option>
                        <option value='São Paulo'>São Paulo</option>
                        <option value='Sergipe'>Sergipe</option>
                        <option value='Tocantins'>Tocantins</option>
                    </select>
                    </div>
                    Selecione algumas imagens
                    <div class='form-group file'>
                            
                        <label for='img1'><img src='imagens/foto.png'></label>
                        <input type='file' id='img1' name='img1'>

                        <label for='img2'><img src='imagens/foto.png'></label>
                        <input type='file' id='img2' name='img2'>

                        <label for='img3'><img src='imagens/foto.png'></label>
                        <input type='file' id='img3' name='img3'>

                        <label for='img4'><img src='imagens/foto.png'></label>
                        <input type='file' id='img4' name='img4'>

                        <label for='img5'><img src='imagens/foto.png'></label>
                        <input type='file' id='img5' name='img5'>
                    </div>
                    <h5>Campos que contenham * são obrigatórios</h5>
                </div>
                
                <!-- rodape -->
                <div class='modal-footer'>
                    <button type='button' class='btn btn-default' data-dismiss='modal'>
                        Cancelar
                    </button>
                    <button type='submit' class='btn botao-modal'>
                        Enviar
                    </button>
                </div>
            </div>
        </div>
    </form>
    <a class='dropdown-item' href='' data-toggle='modal' data-target='#janela'>
        <img src='imagens\add.png' alt='Anunciar' title='Anunciar' class='botao-anuncio'>
    </a>

    ";
   
  }

?>