        <footer id="rodape">              
              <div class="container">
                <div class="row">    
                <div class="col-md-2 menu-rodape">
                    <ul class="nav">
                      <li><h4>Ajuda</h4></li>                      
                      <li><a href="#">Como Funciona</a></li>
                      <li><a href="#">Perguntas Frequentes</a></li>
                      <li><a href="#">Anuncie seu produto</a></li>
                      <li><a href="#">Fale conosco</a></li>
                      <li><a href="#">Termos de uso</a></li>
                    </ul>
                  </div>
                  <div class="col-md-2">
                      <ul class="nav">                    
                      <li><h4>Sobre nós</h4></li>
                      <li><a href="#">Nossa equipe</a></li>
                      <li><a href="#">Missão</a></li>
                      <li><a href="#">Trabalhe Conosco</a></li>
                    </ul>
                  </div>
                  <div class="col-md-3">
                    <ul class="nav">
                      <li><h4>Siga-nos</h4></li>                      
                      <li class="item-rede-social"><a href="#"><i class='fab fa-facebook icone-rede' ></i></a></li>
                      <li class="item-rede-social"><a href="#"><i class='fab fa-twitter icone-rede'></i></a></li>
                      <li class="item-rede-social"><a href="#"><i class='fab fa-google-plus-g icone-rede'></i></a></li>
                      <li class="item-rede-social"><a href="#"><i class='fab fa-instagram icone-rede'></i></a></li>
                    </ul>
                  </div>                 
                </div><!-- /row -->              
              </div>
            </footer>
        <!-- Link com os scripts das bibliotecas -->
        <!-- Jquery -->
        <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>
            <script src="http://code.jquery.com/jquery-lastest.js"></script>
        <!-- Slick -->
            <script src="assets/js/slick/slick.min.js"></script>
        <!-- Bootstrap -->
            <script src='assets/bootstrap/js/bootstrap.min.js'></script>

        <script>
          $(".slideshow").slick({
            autoplay: true,
            dots: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive:
              [ {
                breakpoint: 1024,
                settings: 
                {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
                }},
                {
                  breakpoint: 600,
                  settings: 
                  {
                    slidesToShow: 2,
                    slidesToScroll: 2
                  }},
                  {
                    breakpoint: 480,
                    settings: 
                    {
                      slidesToShow: 1,
                      slidesToScroll: 1
                    }
                  }
              ]
          });

          </script>

          <script>

          $('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: true,
  centerMode: true,
  focusOnSelect: true
});

          </script>
        </body>
        </html>
